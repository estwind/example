let gulp         = require('gulp'),
    browserSync  = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    cache       = require('gulp-cache');

gulp.task('css', function() {
    return gulp.src('src/*.css')
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(browserSync.reload({stream: true}))
});
gulp.task('scripts', function() {
    return gulp.src('src/*.js')
        .pipe(browserSync.reload({stream: true}))
});
gulp.task('code', function() {
    return gulp.src('src/*.html')
        .pipe(browserSync.reload({ stream: true }))
});
gulp.task('img', function() {
    return gulp.src('src/img/')
        .pipe(browserSync.reload({ stream: true }))
});
gulp.task('fonts', function() {
    return gulp.src('src/fonts' +
        '/')
        .pipe(browserSync.reload({ stream: true }))
});
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'src'
        },
        notify: false
    });
});

gulp.task('clear', function (callback) {
    return cache.clearAll();
});

gulp.task('prebuild', async function() {
    let buildCss = gulp.src('src/*.css')
        .pipe(gulp.dest('build/'));

    let buildFonts = gulp.src('src/fonts/*')
        .pipe(gulp.dest('build/fonts/'));

    let buildJs = gulp.src('src/*.js')
        .pipe(gulp.dest('build'));

    let buildHtml = gulp.src('src/*.html')
        .pipe(gulp.dest('build'));

    let buildImg = gulp.src('src/img/*')
        .pipe(gulp.dest('build/img/'));
});

gulp.task('watch', function() {
    gulp.watch('src/*.css', gulp.parallel('css'));
    gulp.watch('src/*.html', gulp.parallel('code'));
    gulp.watch('src/*.js', gulp.parallel('scripts'));
});

gulp.task('default', gulp.parallel('css', 'scripts', 'browser-sync', 'watch'));

gulp.task('build', gulp.parallel('prebuild', 'clear', 'scripts', 'css', 'img', 'fonts'));